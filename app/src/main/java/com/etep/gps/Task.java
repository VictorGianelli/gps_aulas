package com.etep.gps;

public class Task {
    /*
    Ao criar um novo projeto, na tela "Add an Activity to Mobile" escolha "Google Maps Activity".

    Após finalizar, abrirá o xml google_maps_api que diz : Antes de você rodar a sua aplicação, você precisa de uma chave Google Maps API.
    Cole no seu navegador o link exibido. (https://console.developers.google.com/flows/enableapi?apiid=maps_android_backend&keyType=CLIENT_SIDE_ANDROID&r=...)

    No site, escolha o seu pais e marque as opções das duas questoes.
    Então clique em "Criar chave de API"
    Copie a chave exibida então, volte ao AndroidStudio e cole em YOUR_KEY_HERE
    */

    /*
    Aula como configurar o Firebase

    !Atenção: (se der) dê preferência para o Google Chrome como navegador!

    Acessar: https://developer.android.com/studio/write/firebase ()

    1 - Criando projeto no firebase
    1.1 - Acessar: https://console.firebase.google.com/  e logar em sua conta google
    1.2 - Clique em 'Adicionar projeto'.
          Em 'Nome do projeto' pode ser inserido o mesmo do projeto do AndroidStudio(de pelo menos 4 caracteres)
          Clique no 'lápis' perto de 'Locais' para ajustar 'Localização do Analytics' e/ou 'Local do Cloud Firestore'
          Marque 'Confirmo que estou usando os serviços do Firebase no meu app e concordo com os aplicáveis atualizados.'
          Clique em 'Continuar' e depois em 'Criar projeto'.

    2 - Configurando o firebase
    2.1 - No AndroidStudio vá em Tools > SDK Manager
    2.2 - Clique em SDK Tools e (se estiver desmarcado) marque 'Google Repository' e clique em 'Apply' (Aceite os termos e instale)
    2.3 - Após instalar o 'Google Repository', vá em Tools > Firebase e o 'Assistanty' irá abrir.
    2.4 - Clique em 'Realtime Database' e então em 'Save and retrive data' e clique no botão 'Connect to Firebase'
    2.4 - Utilizando o Chrome, logue na conta Google e clique em 'Permitir'
    2.4 - Voltando para o AndroidStudio, irá abrir uma janela.
    2.5 - Selecione 'Choose an existing Firebase or Google project', escolha o nome que você deu para o projeto e clique em 'Connect to Firebase'.
          Vai aparecer o alerta 'Firebase: Connect to Firebase failed', no 'Assistant' clique mais uma vez no botão 'Connect to Firebase' e então clique no botão 'Sync'
    2.6 - Substitua parte do comando 'build.gradle (Project: ...)' por:
            ...
            dependencies {
                classpath 'com.android.tools.build:gradle:3.2.0'


                // NOTE: Do not place your application dependencies here; they belong
                // in the individual module build.gradle files
                classpath 'com.google.gms:google-services:3.2.0'
            }
            ...

    2.7 - Substitua parte do comando 'build.gradle (Module: ...)' por:
            ...
            dependencies {
                implementation fileTree(dir: 'libs', include: ['*.jar'])
                implementation 'com.android.support:appcompat-v7:27.1.1'
                implementation 'com.android.support:customtabs:27.1.1'
                implementation 'com.google.android.gms:play-services-maps:16.0.0'
                implementation 'com.google.firebase:firebase-database:16.0.3'
                testImplementation 'junit:junit:4.12'
                androidTestImplementation 'com.android.support.test:runner:1.0.2'
                androidTestImplementation 'com.android.support.test.espresso:espresso-core:3.0.2'
            }

            apply plugin: 'com.google.gms.google-services'

          Então aprte na parte superior direita 'Sync Now'. Caso ocorra um erro no Gradle, clique na opção 'Fix'

    * */
}
